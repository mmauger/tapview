= tapview(1) =
:doctype: manpage
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: MIT-0

== NAME ==
tapview - a minimalist viewing filter for TAP (Test Anything Protocol).

== SYNOPSIS ==

*tapview* [-s]

[[description]]
== DESCRIPTION ==

tapview accepts a stream of reports in the format of TAP (Test
Anything Protocol) on standard input and filters them for viewing in a
terminal emulator. Its goal is to present to a human viewer only what
the viewer needs to see, and no more - it produces an informative but
extremely compact display even when there are hundreds of tests.

tapview is small, fast, lightweight, and designed to be easily
embedded in any source distribution.  The license explicitly allows it
to be distributed under the terms of the (open-source, OSD-compliant)
license of the embedding project.

The Test Anything Protocol is described here: https://testanything.org/

tapview's output behavior is deliberately simple:

. The plan line may have a SKIP directive; if so, tapview exits
indicating an error without showing any further output.

. The initial section (the "dot display") is displayed in real time
as each TAP line arrives on standard input. It ships one of the
following status characters for each test:
+
|============================================
| . | "ok" line with no directive
| F | "not ok" line with no directive
| s | "ok" or "not ok" line with SKIP directive
| u | "ok" line with TODO directive
| x | "not ok" line with TODO directive
|============================================

. The dot display may be terminated early by a "Bail out!" line. In
this case the rest of the stream is discarded and there is no further
output.

. After the dot display completes, tapview passes through "not ok" lines,
omitting those with a SKIP directive. Not-ok lines with SKIPs or TODOs are
not counted as failures for purposes of setting tapview's return
value.

. Lines beginning with "ok" are ignored, unless the line has a TODO
directive in which case it is considered unexpected and passed
through.

. "Bail out!" lines are passed through, after which an error is returned.

. YAML blocks are passed through following either an "ok" or "not-ok" line.

. tapview silently discards all other input.

. A statistics line is issued after the dot display and any message
  lines passed through.

tapview conforms to TAP version 14
and respects https://testanything.org/philosophy.html[the Tao of TAP],
including nested subtests with plan lines and scoped directives. 
Some notes on "may" and "should" options in the TAP specification follow:

* tapview does not return failure in the absence of a TAP version
  line.

* tapview treats plan lines that do not begin with "1.." as
  ill-formed and reports an error when it sees one.

* tapview ignores SKIP and TODO directives following a comment leader
  that is both not preceded and followed by a space (strictly TAP 14
  conformant).

* tapview does not perform backslash-folding when passing through
  ok and not-ok lines passed through to warn of skip and todo exceptions,

The "bail" and "strict" pragmas from the TAP 14 specification are implemented.

There is one option: -s (streaming) suppresses the complaint about a
missing plan line and also any complaints about multiple plan
lines. With this option, TAP reports from multiple sources can be
concatenated wthout fuss. However, you lose the warning on missing
test lines.

[[requirements]]
== REQUIREMENTS ==

tapview is written in pure POSIX shell. There are no other dependencies.

[[bugs]]
== BUGS AND LIMITATIONS ==

There are ambiguities in the TAP specification that mean the behavior
of parsers is underconstrained. Thus, tapview and other TAP consumers may
treat the same TAP stream differently.

The most notable internal inconsistency is in whether a plan line is
optional or required; tapview considers it required, in accordance
with the TAP version 14 specification.

[[returns]]
== ERROR RETURNS ==

Returns
2 if it saw a "Bail out!" line;
1 on a missing plan line, an ill-formed plan line, or if the expected
number of tests failed to match a single valid plan line, or the
entire test sequence was skipped;
0 otherwise.

[[author]]
== AUTHOR ==

Eric S. Raymond <esr@thyrsus.com>.
The public repository is at https://gitlab.com/esr/tapview[on GitLab].

// end
