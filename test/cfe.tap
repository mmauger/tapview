# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: MIT-0
# Has 54 tests but the plan expects only 52
# Dump regressions
ok - at: Verify parsing of escaped at on final line
ok - basic: basic test for CVS master parsing
ok - branchy: A branchy repo with deletions and only valid tags
ok - daughterbranch: Test for the daughter-branch bug
ok - exec: Test handling of executable bit
ok - expand: Test keyword expansion
ok - hack1: First example from the Hacking Guide
ok - hack2: Second example from the Hacking Guide
ok - hack3: Third example from the Hacking Guide
ok - linear: simplest possible linear repository with multiple commits
ok - longrev: A widely branched repo with long file revision strings.
ok - postbranch: Ilya Basin's test, failed by cvsps-3.x
ok - tagbug: Tricky tag corner case
ok - twobranch: Two-branch repo to test incremental dumping
ok - twotag: A repo with identical tags attached to different changesets
# Master-parsing regressions
ok - access: Master with a nonempty access list.
ok - altexpand: Master with an expand field that's data, not a token.
ok - deadbranch: Master from NetBSD with a branch entirely of dead revisions
ok - emptylabel: Reduced version of an old repo with an empty branch label.
ok - emptytag: Reduced master from NetBSD containing an empty symbol.
ok - empty: Pathological master without trunk from glide3x repository.
ok - hardlinks: Master from gnuplot with hardlinks fields
ok - hashsymbol: Reduced master from NetBSD containing pound-sign in a symbol name
ok - issue22.txt: Test for GitLab issue #22 with trailing semi on its own line.
ok - keywords_default.c: Default keyword expansion
ok - missingbranch: Missing tag for a branch.
ok - noedit: No explicit edit operation in head commit.
ok - nullbranch: This file produces a null branch name
# Repo regressions
ok - oldhead: trunk tip older than vendor branch tip
ok - issue-57: test for GitLab issue #57
# Incremental-dump regressions
ok - twobranch Two-branch repo to test incremental dumping
# Pathological cases
ok - t9601: Test handling of vendor branches
ok - t9602: Test handling of pathological tags
ok - t9603: Testing for correct patchset estimation
ok - t9604: Testing for correct timestamp handling in author maps.
ok - t9605: Testing for correct patchset estimation
# Conversion checks
ok - at.repo conversion compares clean.
ok - basic.repo conversion compares clean.
ok - daughterbranch.repo conversion compares clean.
ok - exec.repo conversion compares clean.
ok - expand.repo conversion compares clean.
ok - hack1.repo conversion compares clean.
ok - hack2.repo conversion compares clean.
ok - hack3.repo conversion compares clean.
ok - longrev.repo conversion compares clean.
ok - postbranch.repo conversion compares clean.
ok - tagbug.repo conversion compares clean.
ok - twobranch.repo conversion compares clean.
ok - oldhead.testrepo conversion compares clean.
ok - t9603.testrepo conversion compares clean.
ok - t9604.testrepo conversion compares clean.
ok - t9605.testrepo conversion compares clean.
ok - vendor.testrepo conversion compares clean.
# Sporadic tests
ok - incremental.sh
1..52
